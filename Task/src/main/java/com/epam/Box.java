package com.epam;

import com.epam.product.Laptop;
import com.epam.product.Phone;
import com.epam.product.Product;

import java.util.ArrayList;
import java.util.List;

public class Box<T> {
    public static void main(String[] args) {
        Phone phone = new Phone("nokia", 200, 100);
        Laptop laptop = new Laptop("toshiba", 500, 200);
        List<? super Product> src = new ArrayList<>();
        src.add(phone);
        src.add(laptop);
        for (Object o : src) {
            System.out.println(o);
        }
        List<? extends Product> dst = new ArrayList<>();
    }

}
