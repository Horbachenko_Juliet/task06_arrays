package com.epam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ComparableString {

    public static final int TEN = 10;

    ArrayList<String> arrayList = new ArrayList<String>();

    public void fillArrayList() {
        Random rnd = new Random();
        System.out.print("Binary arrayList: ");
        for (int i = 0; i < TEN; i++) {
            arrayList.add(Integer.toString(rnd.nextInt(11)));
                System.out.print(arrayList.get(i) + " ");
        }
        System.out.println();
    }

    public int binarySearch(String s) {
        Collections.sort(arrayList);
        int f = 0;
        int l = arrayList.size() - 1;
        while (f <= l) {
            int m = f + (l - f) / 2;
            int res = s.compareTo(arrayList.get(m));
            if (res == 0) {
                return m;
            }
            if (res > 0) {
                f = m + 1;
            } else l = m - 1;
        }
        return -1;
    }
}
