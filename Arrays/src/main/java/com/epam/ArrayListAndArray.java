package com.epam;


import java.util.ArrayList;
import java.util.Date;

public class ArrayListAndArray {
    private int counter = 0;
    private java.lang.String[] strings = new java.lang.String[10];
    ArrayList<String> arrayList = new ArrayList<String>();

    public java.lang.String getString(int index){
        return strings[index];
    }

    public void addStrings(java.lang.String s) {
        if (counter < strings.length){
        strings[counter] = s;
        counter++;
        }else {
            java.lang.String[] newStrings = new java.lang.String[strings.length * 2];
            System.arraycopy(strings, 0, newStrings, 0, strings.length);
            strings = newStrings;
        }
    }

    public void add100kStringsToArray(){
        Date start = new Date();
        for (int i = 0; i < 100000; i++){
            addStrings("mom " + i);
        }
        Date end = new Date();
        System.out.println(end.getTime() - start.getTime());
    }

    public void add100kStringsToList(){
        Date start = new Date();
        for (int i = 0; i < 100000; i++){
            arrayList.add("pep " + i);
        }
        Date end = new Date();
        System.out.println(end.getTime() - start.getTime());
    }
}
