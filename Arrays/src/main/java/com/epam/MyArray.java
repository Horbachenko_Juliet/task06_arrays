package com.epam;

import java.util.*;

public class MyArray {
    private List<Integer> first = new LinkedList<Integer>(Arrays.asList(1, 10, 5, 15, 5, 15, 5, 15, 20, 20));
    private List<Integer> second =  new LinkedList<Integer>(Arrays.asList(10, 10, 5, 5, 15, 15, 15, 15, 20, 20));

    public void createArrayByMatches() {
        List<Integer> matches = new ArrayList<Integer>();
        for (int i = 0; i < first.size(); i++) {
            if (second.contains(first.get(i)) && !matches.contains(first.get(i))) {
                matches.add(first.get(i));
            }
        }
        System.out.print("Matches array: ");
        for (Integer i : matches) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void createArrayByUnMatches() {
        List<Integer> unMatches = new ArrayList<Integer>();
        for (int i = 0; i < first.size(); i++) {
            if (!second.contains(first.get(i)) && !unMatches.contains(first.get(i))) {
                unMatches.add(first.get(i));
            }
        }
        System.out.print("Unmatches array: ");
        for (Integer i : unMatches) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void deleteDuplicateMoreThanTwo() {
        int counter = 0;
        for (int k = 0; k < first.size(); k++) {
            int tmp = first.get(k);
            for (int i = 0; i < first.size(); i++) {
                if (first.get(i).equals(tmp)){
                counter++;}
                if (counter > 2){
                    first.remove(i);
                    counter--;
                    i--;
                }
            }

        }
        System.out.print("Deleted more than two duplicates in array: ");
        for (Integer i : first) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void deleteDuplicates() {
        for (int k = 0; k < second.size() - 1; k++) {
            if (second.get(k).equals(second.get(k + 1))){
                second.remove(second.get(k + 1));
                k--;
            }
        }
        System.out.print("Deleted duplicates array: ");
        for (Integer i : second) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
